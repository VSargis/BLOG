﻿
using System.ComponentModel.DataAnnotations;

namespace Systrotech_Repository.Entities
{
    [MetadataType(typeof(QuestionValidated))]
    public partial class Question
    {
        public string Date { get; set; }
    }
}