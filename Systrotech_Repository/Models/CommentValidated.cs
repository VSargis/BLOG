﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Systrotech_Repository.Entities
{
    class CommentValidated
    {
        [Display(Name ="Comment")]
        public string Comment1 { get; set; }
        
        public DateTime Datetime { get; set; }
        public Nullable<bool> Correct { get; set; }

    }
}
