﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Systrotech_Repository.Entities
{
    public class QuestionValidated
    {

        [Browsable(false)]
        public int Id { get; set; }
        
        [Required]
        [Display(Name = "Title")]
        [StringLength(300, ErrorMessage = "The Maximum length is 300")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Content")]
        [AllowHtml]
        public string Content { get; set; }

        [Required]
        public int CategoryId { get; set; }

        [Required]
        public int UserId { get; set; }

        [Required]
        public System.DateTime Datetime { get; set; }
    }
       
}