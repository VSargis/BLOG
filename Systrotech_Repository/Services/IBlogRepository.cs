﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Systrotech_Repository.Services
{
    public interface IBlogRepository : IDisposable
    {
        List<TModel> SelectAll<TModel>() where TModel : class;
        TModel GetById<TModel>(int id) where TModel : class;
        bool DeleteQuestion(int id);
        bool Insert<TModel>(TModel model) where TModel : class;
        bool Update <TModel>(TModel model) where TModel : class;
    }
}
