﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Systrotech.Blog.Helpers;
using Systrotech.Blog.View_Models;
using Systrotech_Repository.Entities;
using Systrotech_Repository.Services;

namespace Systrotech.Blog.Controllers
{
    public class HomeController : Controller
    {
        BlogRepository blogContext = new BlogRepository();
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LeftBar()
        {
            var categories = blogContext.SelectAll<Category>().OrderBy(p => p.Name).Take(20);
            return PartialView("_LeftBar", categories);
        }

        [ChildActionOnly]
        public ActionResult Home(int id, int categoryId = 0, string searchTerm = null) //id --> "index: 0", "userpage: 1"
        {
            var pageType = id; // view to render in

            TempData["PageType"] = pageType;
            TempData["categoryId"] = categoryId;
            TempData["searchTerm"] = searchTerm;

            HomeViewModel home = new HomeViewModel
            {
                SortByList = Helper.SortList
            };
            return PartialView("_Home", home);
        }

        public ActionResult Question(int SortBy = 1, int UserPage = 0, int Page = 0, int Category = 0, string searchTerm = null)
        {
            TempData["PageType"] = UserPage;
            int pageSize = 10;

            // first filter user data
            var filteredByUserData = blogContext.SelectAll<Question>().ToList();

            if (UserPage == 1)
            {
                try
                {
                    int userId = (int)HttpContext.Session["UserId"];
                    filteredByUserData = filteredByUserData.Where(p => p.UserId == userId).ToList();
                }
                catch (Exception)
                {
                    return RedirectToAction("Login", "Account");
                }
            }

            if (Category > 0)
            {
                filteredByUserData = filteredByUserData.Where(p => p.CategoryId == Category).ToList();
            }

            if (!string.IsNullOrEmpty(searchTerm))
            {
                filteredByUserData = blogContext.SelectAll<Question>().Where(p => p.Title.ToUpper()
                .Contains(searchTerm.ToUpper())).OrderByDescending(p => p.Datetime).ToList();
                if (filteredByUserData.Count==0)
                {
                    ViewBag.message = "There are no results that match your search ";
                }
            }

            #region GetLastPage
            if (filteredByUserData.Count % pageSize == 0) TempData["PageCount"] = filteredByUserData.Count / pageSize - 1;
            else TempData["PageCount"] = filteredByUserData.Count / pageSize;
            #endregion


            IEnumerable<Question> filteredAndSorted = null;
            #region SortBy

            if (SortBy == 1)//byDate
            {
                filteredAndSorted = filteredByUserData.OrderByDescending(p => p.Datetime);
            }
            else if (SortBy == 2)
            {
                filteredAndSorted = filteredByUserData.OrderBy(p => p.Datetime);
            }
            else if (SortBy == 3)//byMost Discussed
            {
                filteredAndSorted = filteredByUserData.OrderByDescending(p => p.Comments.Count());
            }
            else if (SortBy == 4)//byMost View
            {   
                filteredAndSorted = filteredByUserData.OrderByDescending(p => p.Datetime); //TODO: der petq e hashvarkvi sort byMost View
            }
            #endregion

            // apply pagination
            var finalData = Helper.ReplaceQuestionAndConvertDate(filteredAndSorted
                        .Skip(Page * pageSize)
                        .Take(pageSize));

            return PartialView("_Questions", finalData);
        }

    }
}