﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Systrotech.Blog.View_Models;
using Systrotech_Repository.Entities;
using Systrotech_Repository.Services;
using Systrotech.Blog.Helpers;

namespace Systrotech.Blog.Controllers
{
    public class QuestionController : Controller
    {
        BlogRepository blogContext = new BlogRepository();
        int pageSize = 10;
        // GET: Question
        public ActionResult Index(int id)
        {
            int userId = Convert.ToInt32(HttpContext.Session["UserId"]);
            ViewBag.userId = userId;
            var question = blogContext.GetById<Question>(id);

            #region GetLastPage
            if (question.Comments.Count() % pageSize == 0) TempData["PageCount"] = question.Comments.Count() / pageSize - 1;
            else TempData["PageCount"] = question.Comments.Count() / pageSize;
            #endregion

            try // ete delete em anum u pahum em url exceptiona gcum 
            {
                ViewBag.questionUserId = question.UserId;
            }
            catch (Exception)
            {
                return RedirectToAction("Login", "Account");
            }            
            int qCount = blogContext.SelectAll<Question>().Where(p => p.UserId == question.UserId).Count();
            return View(new SelectedQuestionViewModel { Question = question, QuestionsCount = qCount, SortByCommentList = Helper.SortCommentList });
        }

        [HttpPost]
        public ActionResult Comment(SelectedQuestionViewModel model)
        {
            int userId = Convert.ToInt32(HttpContext.Session["UserId"]);
            ViewBag.userId = userId;
            Comment comment = new Comment { Comment1 = model.Comment, Datetime = DateTime.Now, QuestionId = model.Question.Id, UserId = userId };
            comment.Question = blogContext.GetById<Question>(model.Question.Id);
            comment.User = blogContext.GetById<User>(userId);
            bool save = blogContext.Insert(comment);
            return RedirectToAction("CommentList", new { questionId = comment.Question.Id });
        }

        // [HttpPost]
        public ActionResult Search(string searchTerm)
        {
            return View(model: searchTerm);
        }

        public ActionResult SearchByCategory(int id)
        {
            return View("SearchByCategory", id);
        }

        public ActionResult RightQuestion(int categoryId)
        {
            IEnumerable<Question> questionLists = blogContext.SelectAll<Question>()
                .Where(p => p.CategoryId == categoryId)
                .OrderByDescending(p => p.Datetime).Take(5);

            return PartialView("_RightQuestion", questionLists);
        }

        public ActionResult CommentList(int questionId, int sortBy = 1, int page = 0)
        {
            int userId = Convert.ToInt32(HttpContext.Session["UserId"]);
            ViewBag.userId = userId;

            var comments = blogContext.GetById<Question>(questionId).Comments.ToList().OrderByDescending(p => p.Datetime);
            
            #region GetLastPage
            if (comments.Count() % pageSize == 0) TempData["PageCount"] = comments.Count() / pageSize - 1;
            else TempData["PageCount"] = comments.Count() / pageSize;
            #endregion

            if (sortBy == 1)//by date
            {
                comments = comments.OrderByDescending(p => p.Datetime);
            }
            else if (sortBy == 2)//by right answer
            {
                comments = comments.OrderByDescending(p => p.Correct);
            }
            var finalData = comments.Skip(page * pageSize).Take(pageSize);

            return PartialView("_Comment", finalData);
        }

        public ActionResult ChangeComment(int commentId)
        {
            var comment = blogContext.GetById<Comment>(commentId);
            comment.Correct = true;
            bool change=blogContext.Update<Comment>(comment);
            return RedirectToAction("CommentList", new { questionId = comment.Question.Id});
        }
        
        //for autocomplite
        public JsonResult GetQuestions(string term)
        {
            if (!string.IsNullOrEmpty(term))
            {
                try
                {
                    var searchlist = blogContext.SelectAll<Question>().Where(x => x.Title.ToUpper().Contains(term.ToUpper())).Select(p => new { value = p.Id, label = p.Title }).ToList();

                    return Json(searchlist, JsonRequestBehavior.AllowGet);
                }
                catch (Exception)
                {
                    return Json("There are not !!! ", JsonRequestBehavior.AllowGet);
                }
            }
            return Json("There are not !!! ", JsonRequestBehavior.AllowGet);
        }
    }
}