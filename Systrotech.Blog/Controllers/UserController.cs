﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Systrotech.Blog.Helpers;
using Systrotech.Blog.View_Models;
using Systrotech_Repository.Entities;
using Systrotech_Repository.Services;
using System.Linq;

namespace Systrotech.Blog.Controllers
{
    public class UserController : Controller
    {
        private BlogRepository blogContext = new BlogRepository();
        // GET: User
        public ActionResult Question(int? id)
        {
            SelectListItem[] categories = Helper.GetCategories();
            Question question;

            if (id != null && id > 0)
            {
                // edit mode                
                question = blogContext.GetById<Question>((int)id);
            }
            else
            {
                // add mode
                question = new Question();
            }
            return View(new AddQuestionViewModel { Categories = categories, Question = question });
        }

        [HttpPost]
        [ValidateInput(true)]
        public ActionResult Question(AddQuestionViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.Question.Datetime = DateTime.Now;
                if (model.Question.Id != 0)
                {
                    // edit mode->
                    // update db
                    Question question = model.Question;
                    try
                    {
                        question.UserId = (int)HttpContext.Session["UserId"];
                        bool isUpdate = blogContext.Update(question);
                        if (isUpdate)
                        {
                            return RedirectToAction("Page", "User");
                        }
                        else
                        {
                            return RedirectToAction("Question", "User");
                        }
                    }
                    catch (Exception)
                    {
                        return RedirectToAction("Login", "Account");
                    }
                }
                else
                {
                    // add mode
                    // insert db
                    Question question = model.Question;
                    try
                    {
                        question.UserId = (int)HttpContext.Session["UserId"];
                        bool isAdd = blogContext.Insert(question);
                        if (isAdd)
                        {
                            return RedirectToAction("Page", "User");
                        }
                        else
                        {
                            return RedirectToAction("Question", "User");
                        }
                    }
                    catch (Exception)
                    {
                        return RedirectToAction("Login", "Account");
                    }
                }

            }
            return RedirectToAction("Question", "User");
        }

        public ActionResult Page()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            bool isDelete = blogContext.DeleteQuestion(id);
            if (isDelete)
            {
                return PartialView("_Delete");
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
    }
}