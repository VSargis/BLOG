﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Systrotech_Repository.Entities;
using Systrotech_Repository.Services;

namespace Systrotech.Blog.Helpers
{
    public static class Helper
    {

        public static string GetSha1Hash(SHA1 sha1Hash, string input)
        {
            
            byte[] data = sha1Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();            
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }

       
        public static bool VerifySha1Hash(SHA1 sha1Hash, string input, string hash)
        {
            string hashOfInput = GetSha1Hash(sha1Hash, input);
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool Checklogin(string username, string password)// check login
        {
            BlogEntities context = new BlogEntities();
            var user = context.Users.FirstOrDefault(x => x.UserName.ToUpper() == username.ToUpper());// && x.Password == password);

            if (user == null) return false;

            SHA1 sha1Hash = SHA1.Create();
            bool isLogin = Helper.VerifySha1Hash(sha1Hash, password, user.Password);

            if (isLogin)
            {
                HttpContext.Current.Session["UserId"] = user.Id;
                HttpContext.Current.Session["Name"] = user.Name;
                HttpContext.Current.Session["username"] = username;
            }

            return isLogin;
        }

        public static SelectListItem[] GetCategories()
        {
            BlogRepository context = new BlogRepository();
            List<Category> allCategory = context.SelectAll<Category>();
            SelectListItem[] categories = new SelectListItem[allCategory.Count];

            for (int i = 0; i < allCategory.Count; i++)
            {
                categories[i] = new SelectListItem { Text = $"{allCategory[i].Name}", Value = $"{allCategory[i].Id}" };
            }
            return categories;

        }

        public static IEnumerable<Question> ReplaceQuestionAndConvertDate(IEnumerable<Question> questions)
        {
            foreach (var item in questions)
            {
                item.Date = Helper.ConvertDate(item);
                item.Content = Regex.Replace(item.Content, "<.*?>", string.Empty).Replace("&nbsp;", " ").Replace("&mdash;", "—").Replace("&rsquo;","'");
                if (item.Content.Length > 100)
                    item.Content = item.Content.Substring(0, 100) + "...";
            }
            return questions;
        }

        public static string ConvertDate(Question ob)
        {
            string str = null;
            double date = (DateTime.Now - ob.Datetime).TotalMinutes;

            if (date >= 0 && date < (int)DateEnum.Hour)
            {
                str = ((int)date + 1).ToString() + " min ago";
            }

            else if (date >= (int)DateEnum.Hour && date < (int)DateEnum.Day)
            {
                str = ((int)date / (int)DateEnum.Hour + 1).ToString() + " hour ago";
            }

            else if (date >= (int)DateEnum.Day && date < (int)DateEnum.Month)
            {
                str = ((int)date / (int)DateEnum.Day + 1).ToString() + " day ago";
            }

            else if (date >= (int)DateEnum.Month && date < (int)DateEnum.Year)
            {
                str = ((int)date / (int)DateEnum.Month + 1).ToString() + " month ago";
            }
            else if (date >= (int)DateEnum.Year)
            {
                str = ((int)date / (int)DateEnum.Year + 1).ToString() + " year ago";
            }

            return str;
        }


        public static IEnumerable<SelectListItem> SortList
        {
            get
            {
                List<SelectListItem> list = new List<SelectListItem>
                {
                    new SelectListItem { Text="Date Desc",Value="1", },
                    new SelectListItem { Text="Date Asc",Value="2" },
                    new SelectListItem { Text="Most Discussed",Value="3" },
                    new SelectListItem { Text="Most Viewed",Value="4" }
                };
                return list.Select(l => new SelectListItem { Selected = (l.Value == "1"), Text = l.Text, Value = l.Value });
            }
        }

        public static IEnumerable<SelectListItem> SortCommentList
        {
            get
            {
                List<SelectListItem> list = new List<SelectListItem>
                {
                    new SelectListItem { Text="Date",Value="1", },
                    new SelectListItem { Text="Right Answer",Value="2" },
                };
                return list.Select(l => new SelectListItem { Selected = (l.Value == "1"), Text = l.Text, Value = l.Value });
            }
        }
    }
}

enum DateEnum
{
    Minute = 1,
    Hour = Minute * 60,
    Day = Hour * 24,
    Week = Day * 7,
    Month = Day * 30,
    Year = Month * 12
}
