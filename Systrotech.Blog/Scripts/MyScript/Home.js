﻿var page = 0;
var QuestionId = null;

var actions={
    PostSucceed: function (DataReturnedFromServer) {
        $("#divQuestionContent").empty().append(DataReturnedFromServer);
        $("#divQuestionContent").css("background-color", "blanchedalmond");
        setTimeout(function(){$("#divQuestionContent").css("background-color","")}, 600);
    },

    Browse: function (pageToMove) {
        switch (pageToMove) {
            case 'first':
                page=0;
                break;
            case 'prev':
                if(page>0)
                    page--;
                break;
            case 'next':
                if(page <currentPage)
                    page++;
                break;
            case 'last':
                page = currentPage;
                break;
        }

        var item = $("#SortBy").val();

        $.post(homeQuestionAction, { 'SortBy': item, 'UserPage': pageType, 'Page': page, 'Category': categoryId, 'searchTerm': bySearch }, actions.PostSucceed).error(function (e, d, r) {
            alert('error');
        });
    },
}

var questions = {

    GetId: function (e) {
        QuestionId = $(e).data("index");
    },

    Delete: function () {

        $.post(deleteAction, { 'id': QuestionId }, actions.PostSucceed).error(function (e, d, r) {
            alert('error');
        });
    },

}

 var changeComment = {

        correct: function (commentId) {
            $.post(questionChangeComment, { 'commentId': commentId }, comments.PostSucceed).error(function (e, d, r) {
                alert('error');
            });
        },
 }
 
var comments={
        PostSucceed: function (DataReturnedFromServer) {
            $("#comment").empty().append(DataReturnedFromServer);
            $("#comment").css("background-color", "blanchedalmond");
            setTimeout(function(){$("#comment").css("background-color","")}, 600);
        },

        Browse: function (pageToMove) {
            switch (pageToMove) {
                case 'first':
                    page=0;
                    break;
                case 'prev':
                    if(page>0)
                        page--;
                    break;
                case 'next':
                    if(page <currentPage)
                        page++;
                    break;
                case 'last':
                    page = currentPage;
                    break;
            }

            var item = $("#SortByComment").val();

            $.post(questionCommentList, { 'sortBy': item,'questionId': questionId,'page': page}, comments.PostSucceed).error(function (e, d, r) {
                alert('error');
            });
        },
    }

 function success(result) {
        $("#comment").empty().append(result);
        $("#comment").css("background-color", "blanchedalmond");
        setTimeout(function () { $("#comment").css("background-color", "") }, 600);
    } //for add comment 

 $(function () {
        $("#SortByComment").kendoDropDownList();

        $("#SortByComment").change(function (e) {
            var item = $("#SortByComment").val();

            $.post(questionCommentList, { 'sortBy': item, 'questionId': questionId }, comments.PostSucceed).error(function (e, d, r) {
                alert('error');
            });
        });
 });

 $(function () {
     $("#editor").kendoEditor({

         value: $("#Question_Content").val()
     });

     $("#btnSubmit").on('click', function (e) {
         var editor = $("#editor").data("kendoEditor");
         var text = editor.value();
         $("#Question_Content").val(text);
     });

     $("#cmbCategories").kendoDropDownList();
 });

 $(function () {
    $("#SortBy").kendoDropDownList();

    $("#SortBy").change(function (e) {
        var item = $("#SortBy").val();

        $.post(homeQuestionAction, { 'SortBy': item, 'UserPage': pageType, 'searchTerm': bySearch }, function (e) {
            actions.PostSucceed(e);
            page=0;
        }).error(function (e, d, r) {
            alert('error');
        });
    });
});
