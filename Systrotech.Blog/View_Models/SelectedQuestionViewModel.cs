﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Systrotech_Repository.Entities;

namespace Systrotech.Blog.View_Models
{
    public class SelectedQuestionViewModel
    {
        public Question Question{ get; set; }
        public string Comment { get; set; }
        public int QuestionsCount { get; set; }

        public IEnumerable<SelectListItem> SortByCommentList { get; set; }
        public int SortByComment { get; set; }


    }
}