﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Systrotech_Repository.Entities;

namespace Systrotech.Blog.View_Models
{
    public class AddQuestionViewModel
    {
        public Question Question { get; set; }

        [Display(Name = "Category")]
        public IEnumerable<SelectListItem> Categories { get; set; }

    }
}