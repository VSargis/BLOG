﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Systrotech.Blog.View_Models
{
    public class HomeViewModel
    {
        public IEnumerable<SelectListItem> SortByList { get; set; }
        public int SortBy { get; set; }

    }
}